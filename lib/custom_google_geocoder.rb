class CustomGoogleGeocoder
  @maps_api_uri = "https://maps.googleapis.com/maps/api"

  def self.coordinates(address)
    result = Faraday.new(@maps_api_uri).get "geocode/json?address=#{address}"
    response = JSON.parse(result.body)["results"]

    return nil if response.empty?
    response.first.dig("geometry", "location")
  end
end
