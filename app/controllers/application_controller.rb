class ApplicationController < ActionController::API
  def render_json_error(status, error_message)
    status = Rack::Utils::SYMBOL_TO_STATUS_CODE[status] if status.is_a? Symbol

    error = {
        title: error_message,
        status: status
    }

    render json: { errors: [error] }, status: status
  end
end
