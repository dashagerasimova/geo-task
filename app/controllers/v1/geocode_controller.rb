module V1
  class GeocodeController < ApplicationController
    def show
      geocode = Rails.cache.fetch(geocode_params[:address], expires_in: 12.hours) do
        CustomGoogleGeocoder.coordinates(geocode_params[:address])
      end

      if geocode.present?
        render json: geocode
      else
        render_json_error 404, "Geocode not found"
      end
    end

    def geocode_params
      params.permit(:address)
    end
  end
end
