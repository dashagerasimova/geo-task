# Тестовое задание Сервис геокодирования

Пример запроса:
```
curl -X GET  \ 
     -H 'Content-Type: application/json' \
     -H 'Accept: application/json' \
     'http://localhost:3000/v1/geocode?address=Казань ул Нариманова д. 66'     
```
Ответ: {"lat": 10, "lon": 10}