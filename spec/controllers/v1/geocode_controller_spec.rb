require "rails_helper"

RSpec.describe V1::GeocodeController, type: :controller do
  describe "GET #show" do
    context "when user tries to enter valid address" do
      it "is successful" do
        get :show, params: { address: "Нариманова" }
        expect(response).to have_http_status(:success)
      end

      it "returns valid geocoordinates" do
        get :show, params: { address: "Нариманова" }
        expect(JSON.parse(response.body)).to eq("lat" => 55.5774689, "lng" => 39.5432097)
      end
    end

    context "when user tries to enter invalid address" do
      it "is not successful" do
        get :show, params: { address: "felkjsef" }
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
