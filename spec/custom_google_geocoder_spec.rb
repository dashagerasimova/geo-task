require "rails_helper"

describe CustomGoogleGeocoder do
  describe ".coordinates" do
    context "with valid address" do
      it "returns geocode" do
        expect(CustomGoogleGeocoder.coordinates("Нариманова")).to eq("lat" => 55.5774689, "lng" => 39.5432097)
      end
    end

    context "with invalid address" do
      it "returns nil" do
        expect(CustomGoogleGeocoder.coordinates("aiusfyiusdhyiuds")).to eq(nil)
      end
    end

    context "given an empty string" do
      it "returns nil" do
        expect(CustomGoogleGeocoder.coordinates("")).to eq(nil)
      end
    end
  end
end