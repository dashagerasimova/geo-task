# Load the Rails application.
require_relative "application"

# Initialize the Rails application.
Rails.application.initialize!

Encoding.default_internal = Encoding::UTF_8
